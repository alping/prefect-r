# Running R Scripts with Prefect

```shell
conda env create -n prefect-r --file env.yml
conda activate prefect-r

cd src
python flow.py
```
