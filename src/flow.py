"""Example Prefect flow for working with R scripts."""

from pathlib import Path
from pandas import DataFrame
from prefect import task, Flow

from r import RTask

TEMPDIR = Path("../data/temp").absolute()

rtask = RTask(TEMPDIR, clean_temp=True)


@task
def generate_data() -> DataFrame:
    return DataFrame(
        {
            "a": [0, 1, 2, 3, 4],
            "b": [5, 6, 7, 8, 9],
        }
    )


@task
def print_result(df: DataFrame) -> None:
    print(df)


if __name__ == "__main__":
    with Flow("Prefect-R Flow") as flow:
        data = generate_data()

        r_data = rtask("./r.R", data=data, col="c", number=10)

        print_result(r_data)

    flow.run()
