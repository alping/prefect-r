"""Task and Functions for running R scripts with Prefect."""

from pathlib import Path
from subprocess import Popen, PIPE, STDOUT, CalledProcessError
from typing import Callable
import uuid

import pandas as pd
from pandas import DataFrame, Series
from prefect import Task


def run_in_shell(cmd: str, log_function: Callable = None) -> None:
    """Runs `cmd` using Python's subprocess module and logs the output with `log_function`.

    Args:
        cmd (str): Command to be run.
        log_function (Callable, optional): Function to log output. Defaults to None.

    Raises:
        CalledProcessError: If subrocess ends with returncode != 0.
    """
    with Popen(cmd, stdout=PIPE, stderr=STDOUT) as p:
        if callable(log_function):
            for line in p.stdout:
                text = line.decode().strip()
                if text != "":
                    log_function(text)

    if p.returncode:
        raise CalledProcessError(p.returncode, cmd)


def rscript(r_file: str | Path, *args, log_function: Callable = None) -> None:
    """Runs an R-file with rscript.

    Args:
        file (str | Path): R-file to be run.
        log_function (Callable, optional): Function to log output. Defaults to None.
    """
    if callable(log_function):
        log_function(f"Running rscript: {[r_file, *args]}")

    run_in_shell(["rscript", r_file, *args], log_function=log_function)


class RTask(Task):
    """
    Run R scripts and return the resulting dataset.

    Data are move between Python and R in .feather temp files.
    """

    def __init__(self, temp_dir: str | Path, clean_temp: bool = True, **kwargs) -> None:
        """Initialize the task.

        Args:
            temp_dir (str | Path): Path to the temp directory for storing intermediate files
            clean_temp (bool, optional): Clean temp directory after run. Defaults to True.
        """
        self.temp_dir = Path(temp_dir).resolve()
        self.clean_temp = clean_temp

        # If temp_dir doesn't exist, create it
        self.temp_dir.mkdir(parents=True, exist_ok=True)

        super().__init__(**kwargs)

    def run(self, r_file: str | Path, **kwargs) -> DataFrame | None:
        """Run the task by running the R file with the supplied arguments.

        Args:
            r_file (str | Path): R file to run

        Returns:
            DataFrame | None: Resulting dataframe if any
        """
        r_file = Path(r_file).resolve()

        if not r_file.exists():
            raise ValueError(f"r_file does not exist: {r_file}")

        # Make sure the name of the temp file is unique by adding an uuid before the file extension
        ext = f".{uuid.uuid4()}.feather"

        r_args = []
        temp_in = []
        temp_out = self.temp_dir.joinpath(r_file.stem).with_suffix(ext)
        data: DataFrame | None = None

        for n, arg in kwargs.items():
            # If arg is a DataFrame or Series, write to a tempfile and keep location as
            # arg. Reset index as feather can't handle index metadata. Convert any other
            # args to strings.
            if isinstance(arg, DataFrame) or isinstance(arg, Series):
                data_temp = self.temp_dir.joinpath(n).with_suffix(ext)
                arg.reset_index().to_feather(data_temp)
                r_args.append(data_temp.as_posix())
                temp_in.append(data_temp.as_posix())
            else:
                r_args.append(str(arg))

        # Run the R script
        rscript(r_file.as_posix(), *r_args, temp_out.as_posix(), log_function=self.logger.info)

        # Read the temp data as a Pandas dataframe
        if temp_out.exists():
            data = pd.read_feather(temp_out)

        # Remove temp file
        if self.clean_temp:
            for temp_file in [temp_out, *temp_in]:
                Path(temp_file).unlink(missing_ok=True)

        return data
